// Boot loader.
//
// Part of the boot block, along with bootasm.S, which calls bootmain().
// bootasm.S has put the processor into protected 32-bit mode and calls
// helloC() function. helloC() just prompts.
#include "types.h"
#include "x86.h"
#include "memlayout.h"

void ttyputchar(char c) {
  #define CRTPORT 0x3d4
  ushort *crt = (ushort*)0xb8000;  // CGA memory
  int pos;

  // get cursor position: col + 80*row.
  outb(CRTPORT, 14);
  pos = inb(CRTPORT+1) << 8;
  outb(CRTPORT, 15);
  pos |= inb(CRTPORT+1);

  // put character on screen
  crt[pos++] = (c & 0xff) | 0x0700;

  // update cursor position and put a space (move right)
  outb(CRTPORT, 14);
  outb(CRTPORT+1, pos>>8);
  outb(CRTPORT, 15);
  outb(CRTPORT+1, pos);
  crt[pos] = ' ' | 0x0700;
}

void prompt()
{
	ttyputchar('O');
	ttyputchar('K');
	ttyputchar('!');
}
