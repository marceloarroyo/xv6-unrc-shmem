// Simple shared memory
// Process doing shmget(key) syscall (see shmwriter.c, shmreader.c)
// get an int representing a virtual address of a share memory block
// which can be casted to a pointer of any type
// In this version, shmget allocate only one page (4KB).
// Shared memory blocks have a process reference counter, so it is 
// freed on exit of last process using it (see exit() in proc.c).
// This project required to some changes in vm.c:
// 	1. function mappages() was made public
// 	2. unmappages() was added

#include "types.h"
#include "x86.h"
#include "defs.h"
#include "date.h"
#include "param.h"
#include "memlayout.h"
#include "mmu.h"
#include "proc.h"
#include "spinlock.h"

static struct {
	struct spinlock lock;
	struct shmblock blocks[NSHM];
} shmtable;

void shminit(void)
{
  initlock(&shmtable.lock, "shmtable");
}

// compute max virtual address of process
static uint maxva(struct proc* p)
{
	int slot;
	uint max = p->sz;

	for (slot=0; slot<NOSHM; slot++)
		if (p->shm[slot].va > max)
			max = p->shm[slot].va;
	return max;
}

// return a virtual address of shared memory block (1 page)
int shmget(int key)
{
	int i, slot;
	struct proc* p = myproc();
	uint va = maxva(p);

	// search for free slot in p->shm array
	for(slot=0; slot<NOSHM && p->shm[slot].block; slot++)
		;
	if (slot == NOSHM)
		// no free slot
		return -1;

	acquire(&shmtable.lock);
	for (i=0; i<NSHM && key != shmtable.blocks[i].key; i++)
		;
	if (i == NSHM) {
		// not found: found free block descriptor and alloc memory 
		cprintf("Creating new shmem block...\n");
		for (i=0; i<NSHM && shmtable.blocks[i].refcount != 0; i++) 
			;
		if (i < NSHM) {
			char * frame = kalloc();
			shmtable.blocks[i].pa = frame;
			memset(frame, 0, PGSIZE);
			if (shmtable.blocks[i].pa == 0)
				va = 0; // out of memory
			else {
				shmtable.blocks[i].key = key;
			}
		} else {
			va = 0;	// no free shared memory descriptors
		}
	}	
	
	if (va) {
		cprintf("shmem: Mapping va=%p to pa=%p\n", va, V2P(shmtable.blocks[i].pa));
		shmtable.blocks[i].refcount++;
		mappages(p->pgdir, (void*)va, PGSIZE, V2P(shmtable.blocks[i].pa), PTE_W|PTE_U);
		p->shm[slot].va = va;
		p->shm[slot].block = &(shmtable.blocks[i]);
	}	
	release(&shmtable.lock);
	cprintf("shmem: va=%d\n", va);
	return (int)va;
}

void shmfree(struct proc *p, int i)
{
	if (--p->shm[i].block->refcount == 0) {
	  deallocuvm(p->pgdir, p->shm[i].va + PGSIZE, p->shm[i].va);
	  p->shm[i].block->key = 0;
	  p->shm[i].block->pa = 0;
	}
	else
	  unmappages(p->pgdir, p->shm[i].va, PGSIZE);
	p->shm[i].block = 0;
	p->shm[i].va = 0;
}
