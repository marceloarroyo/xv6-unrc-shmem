#include "types.h"
#include "user.h"

// static char *msg = "Hello world";

int main(void)
{
	int * ptr = (int*) shmget(123456);

	printf(1,"ptr: %d\n", ptr);
	// parent write to shared memory
	ptr[0] = 10;
	ptr[1] = 20;
	ptr[2] = 30;
	printf(1,"writer: %d %d %d\n", ptr[0], ptr[1], ptr[2]);
	
	if (fork() == 0) {
		char * cmd[] = {"shmreader", 0};

		exec(cmd[0], cmd);
	}
	wait();
	printf(1,"writer exiting...\n");
	exit();
}
